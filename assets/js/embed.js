$(document).ready(() => {
    $('.media a').click(event => {
        const el = event.currentTarget
        const href = el['href'] || ''
        const ytPrefix = 'https://youtube.com/watch?v='
        const params = (href.split(ytPrefix)[1] || '')
            .split('&')
        const videoId = params[0]
        const url = `https://youtube.com/embed/${videoId}?${params.slice(1).join('&')}&`
        const isSupported = href.startsWith(ytPrefix) && !!videoId
        // console.log({ url, href, videoId, isSupported })
        if (!isSupported) return

        event.preventDefault()

        const modalEl = $('.modal.embed-container')
        const imgEl = [...el.children]
            .find(child => child.tagName === 'IMG')
        const placeholder = imgEl
            ? $(el).html()
            : `<div style="display:block; text-align: center; padding: 150px; color: black">Loading...</div>`

        modalEl.addClass('mini')
        modalEl.html(`<div class="ui embed basic">${placeholder}</div>`)
        modalEl.modal('setting', 'onHidden', () => modalEl.html(''))

        $(`.ui.modal.embed-container .ui.embed`)
            .embed({
                // source: 'youtube',
                // id: videoId,
                url,
                autoplay: true,
                // onDisplay: () => {},
                // onEmbed: () => {},
            })

        modalEl.modal('show')
        setTimeout(() => {
            modalEl.removeClass('mini')
        }, 1000)
    })
})