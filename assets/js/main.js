// crownload counter related variables
const startDate = new Date('2022-07-18T10:00:00Z') // use `null` for coming-soon
const endDate = new Date('2022-08-08T09:59:59Z') // ignored if startDate is not set
const min = 1000 * 60
const hour = min * 60
const day = hour * 24
const month = day * 30
const statuses = [
    'active',
    'closed',
    'coming-soon',
    'pending'
]

const autoUpdateCounter = delay => setTimeout(() => {
    let status = statuses[2]
    const now = new Date()
    let targetDate
    if (now < startDate) {
        status = statuses[3]
        targetDate = startDate
    } else if (startDate && now > startDate && now < endDate) {
        status = statuses[0]
        targetDate = endDate
    } else if (startDate && now > endDate) {
        status = statuses[1]
    }
    /*
     * Set crowdloan status CSS class
     * :: Valid classes ::
     * crowdloan-coming-soon : specific date hasn't been announced yet
     * crowdloan-pending     : specific date announced and hasn't started yet. (shows a countdown timer)
     * crowdloan-active      : crowdloan is currently active. (shows countdown timer)
     * crowdloan-closed      : crowdloan is closed
     */
    const classList = document.querySelector('body').classList
    classList.add(`crowdloan-${status}`)
    statuses.filter(s => s !== status)
        .forEach(s => classList.remove(`crowdloan-${s}`))

    if (!targetDate) return

    const selectorPrefix = '.timer.container .value.column'
    const diff = Math.abs(targetDate - now)
    const months = parseInt(diff / month)
    let sub = months * month
    const days = parseInt((diff - sub) / day)
    sub += days * day
    const hours = parseInt((diff - sub) / hour)
    sub += hours * hour
    const mins = parseInt((diff - sub) / min)
    // console.log({ months, days, hours, mins })
    const fillZero = num => `${num}`.padStart(2, '0')

    document.querySelector(`${selectorPrefix}.month`).textContent = fillZero(months)
    document.querySelector(`${selectorPrefix}.day`).textContent = fillZero(days)
    document.querySelector(`${selectorPrefix}.hour`).textContent = fillZero(hours)
    document.querySelector(`${selectorPrefix}.minute`).textContent = fillZero(mins)

    autoUpdateCounter(60000)
}, delay)

/**
 * @name	deferred
 * @summary returns a function that invokes the callback function after certain delay/timeout
 * 
 * @param	{Function}	callback 	function to be invoked after timeout
 * @param	{Number}	delay		(optional) timeout duration in milliseconds.
 * 									Default: 50
 * @param	{*}			thisArg		(optional) the special `thisArgs` to be used when invoking the callback.
 * 
 * @returns {Function}
 */
const deferred = (callback, delay, thisArg) => {
    if (!callback) return // nothing to do!!
    let timeoutId
    return (...args) => {
        if (timeoutId) clearTimeout(timeoutId)
        timeoutId = setTimeout(() => callback.apply(thisArg, args), delay || 50)
    }
}

// initialize everything make sure if anything fails other functions are not affected 
const init = () => [
    autoUpdateCounter,
    setupFooter,
    setupFloatingItems,
    setupGifAnimation,
    setupMobileSidebar,
    setupOnClickScroll,
].map(func => setTimeout(() => {
    try {
        func()
    } catch (err) {
        console.error(err)
    }
}, 100))

const setupFloatingItems = () => {
    const onScollItems = [
        '.fixed.menu', // floating menu
        '.scroll-to-top', // scroll to top button
    ]
    // On-scroll down below the hero section, show the floating menu
    $('.masthead').visibility({
        once: false,
        onBottomPassed: () => setTimeout(() => {
            onScollItems.forEach(selector => {
                $(selector).transition('fade in')
                // document.querySelector(selector).classList.remove('hidden')
            })
        }, 100),
        onBottomPassedReverse: () => {
            onScollItems.forEach(selector => {
                $(selector).transition('fade out')
                // document.querySelector(selector).classList.add('hidden')
            })
        },
    })

    // on-click listener for the floating scroll to top button
    $('.scroll-to-top').click(() =>
        window.scroll({ top: 0, behavior: 'smooth' })
    )
}

const setupFooter = () => {
    // copy mobile sidebar menu to generate desktop footer menu
    const cloned = document.querySelector('.mobile.sidebar').cloneNode(true)
    cloned.classList.remove('mobile', 'sidebar', 'right')
    cloned.classList.add('inverted', 'row')
    document.querySelector('.footer.segment .row').replaceWith(cloned)
    // Remove down arrows from footer menu headers
    Array.from(document.querySelectorAll('.footer.segment .row > .item'))
        .forEach(x => x.classList.remove('active'))
    Array.from(document.querySelectorAll('.footer.segment .icon.angle'))
        .forEach(x => x.remove())
}

const setupGifAnimation = () => {
    // on scroll enable gif animation
    $(document).on('scroll', deferred(function () {
        const scrollTop = $(document).scrollTop()
        const windowHeight = $(window).height()
        const elements = [...$('.image-swap')]
        elements.forEach(el => {
            const top = $(el).offset().top
            const bottom = top + $(el).height()
            const height = $(el).height()
            const inView = (scrollTop + windowHeight) > (top + height / 3)
                && scrollTop < bottom
            const isPlaying = el.classList.contains('swapped')
            // start playing
            inView && !isPlaying && el.click()
            // stop playing
            !inView && isPlaying && el.click()
        })
    }))

    // Swap image source on hover and click => play/pause Gif on click
    $('.image-swap').click(e => {
        const el = e.currentTarget
        const isSwapped = el.classList.contains('swapped')
        el.classList[isSwapped ? 'remove' : 'add']('swapped')

        // swap image src
        const imageEl = [...$(el).children()]
            .find(x => x.classList.contains('image'))
        const src = imageEl.getAttribute('src')
        imageEl.setAttribute('src', imageEl.getAttribute('srcAlt'))
        imageEl.setAttribute('srcAlt', src)

        // swap play-pause icon
        const iconEl = [...$(el).children()]
            .find(x => x.classList.contains('overlay'))
            .children[0]
        iconEl.classList[isSwapped ? 'add' : 'remove']('play')
        iconEl.classList[!isSwapped ? 'add' : 'remove']('pause')
    })
}

const setupMobileSidebar = () => {
    // create sidebar and attach to menu open
    $('.ui.sidebar').sidebar('attach events', '.toc.item')
    window.sidebar = $('.ui.sidebar')

    // Mobile: close sidebar on sidebar menu item click
    $('.sidebar.mobile a').click(event => {
        $('.sidebar.menu.mobile .icon-close').click()
    })

    $('.sidebar.menu.mobile .icon-close').click(event => {
        $('.masthead .toc.item').click()
        $('.fixed.menu').css({ marginTop: 0 })
    })

    $('.fixed.menu .toc.item').click(event => {
        $('.fixed.menu').css({ marginTop: '-100%' })
    })

    // toggle mobile sidebar item active status on-click
    $('.mobile.sidebar > .item').click(e => {
        const isActive = e.currentTarget.classList.contains('active')
        Array.from($('.mobile.sidebar > .item'))
            .forEach(x => x.classList.remove('active'))
        e.currentTarget.classList[isActive ? 'remove' : 'add']('active')
    })
}

// Adds smooth scroll to internal navigation.
// Requires "data-scroll-to" attrutube to have the element ID where it should scroll to.
const setupOnClickScroll = () => {
    $('[data-scroll-to]').click(e => {
        let scrollTo = e.currentTarget.attributes['data-scroll-to']
        scrollTo = document.getElementById(scrollTo && scrollTo.value)
        if (!scrollTo) return

        e.preventDefault()
        window.scroll({ top: scrollTo.offsetTop, behavior: 'smooth' })
    })
}

// Initialize
$(document).ready(init)
